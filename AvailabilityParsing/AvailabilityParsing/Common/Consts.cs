﻿namespace AvailabilityParsing.Common
{
    public static class Consts
    {
        public const int MAX_LENGHT_OF_STAY = 7;
        public const int MIN_LENGHT_OF_STAY = 1;
        public const int MAX_AVAILABILITY_MONTHS = 2;
    }
}

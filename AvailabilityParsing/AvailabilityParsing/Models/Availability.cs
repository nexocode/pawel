﻿namespace AvailabilityParsing.Models
{
    using Newtonsoft.Json;
    public partial class Availability
    {
        [JsonProperty("period")]
        public Period Period { get; set; }
    }

}

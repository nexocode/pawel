﻿namespace AvailabilityParsing.Models
{
    using Newtonsoft.Json;
    using System;

    /// <summary>
    /// Check in checkout rules for given period
    /// </summary>
    public partial class Cico
    {
        [JsonProperty("check_in_allowed")]
        public CheckAllowed CheckInAllowed { get; set; }

        [JsonProperty("check_out_allowed")]
        public CheckAllowed CheckOutAllowed { get; set; }

        [JsonProperty("end_date")]
        public DateTime EndDate { get; set; }

        [JsonProperty("start_date")]
        public DateTime StartDate { get; set; }
    }

}

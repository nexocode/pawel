﻿namespace AvailabilityParsing.Models
{
    using AvailabilityParsing.Extensions;
    using Newtonsoft.Json;
    using System;

    public class AvailabilityParsingJsonConverter : JsonConverter
    {
        public override bool CanConvert(Type t) =>  t == typeof(PeriodType)||  t == typeof(PeriodType?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (t == typeof(PeriodType))
                return AvailabilityParsingExtensions.ReadJson(reader, serializer);
       
            if (t == typeof(PeriodType?))
            {
                if (reader.TokenType == JsonToken.Null) return null;
                return AvailabilityParsingExtensions.ReadJson(reader, serializer);
            }

            throw new AvailabilityParsingEnumConverterException($"PeriodTypeConverter Unknown type {t}");
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var t = value.GetType();
                   
            if (t == typeof(PeriodType))
            {
                ((PeriodType)value).WriteJson(writer, serializer);
                return;
            }
            throw new AvailabilityParsingEnumConverterException("Unknown type");
        }

        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = { new AvailabilityParsingJsonConverter() },
        };
    }

}

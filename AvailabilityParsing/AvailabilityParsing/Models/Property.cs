﻿namespace AvailabilityParsing.Models
{
    using AvailabilityParsing.Interfaces;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System;
    using System.Linq;
    using AvailabilityParsing.Services;

    public partial class Property: IAvailabilityProduct
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("availability")]
        public List<Availability> Availability { get; set; }

        [JsonProperty("cico")]
        public List<Cico> Cico { get; set; }

        public ILookup<DateTime, int> TransformToAvailabilities(DateTime? maxDateTime = null)
        {
            return TransformToAvailabilitiesService.TransformToAvailabilities(Availability, Cico, maxDateTime);
        }
    }
}

﻿namespace AvailabilityParsing.Models
{
    using Newtonsoft.Json;
    using System;

    public partial class Period
    {
        [JsonProperty("period_end")]
        public DateTime PeriodEnd { get; set; }

        [JsonProperty("period_start")]
        public DateTime PeriodStart { get; set; }

        [JsonProperty("type")]
        [JsonConverter(typeof(AvailabilityParsingJsonConverter))]
        public PeriodType Type { get; set; }
    }

    public enum PeriodType
    {
        NotAvailable,
        Available
    };
}

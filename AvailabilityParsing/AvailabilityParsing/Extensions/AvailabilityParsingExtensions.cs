﻿namespace AvailabilityParsing.Extensions
{
    using AvailabilityParsing.Models;
    using Newtonsoft.Json;

    static class AvailabilityParsingExtensions
    {
        public static PeriodType ReadJson(JsonReader reader, JsonSerializer serializer)
        {
            switch (serializer.Deserialize<string>(reader))
            {
                case "NotAvailable": return PeriodType.NotAvailable;
                case "Available": return PeriodType.Available;
            }
            throw new AvailabilityParsingEnumConverterException($"Unknown PeriodTypeExtensions enum value= {serializer.Deserialize<string>(reader)}");
        }

        public static void WriteJson(this PeriodType value, JsonWriter writer, JsonSerializer serializer)
        {
            switch (value)
            {
                case PeriodType.NotAvailable: serializer.Serialize(writer, "NotAvailable"); break;
            }
        }
    }

}

﻿using AvailabilityParsing.Models;
using System;

namespace AvailabilityParsing.Extensions
{
    public static class DateTimeExtensions
    {
        public static bool IsDayAllowed(this DateTime datetime, CheckAllowed checkAllowed)
        {
            if(checkAllowed == null)
            {
                return true;
            }

            switch(datetime.DayOfWeek)
            {
                case DayOfWeek.Monday: return checkAllowed.Monday;
                case DayOfWeek.Tuesday: return checkAllowed.Tuesday;
                case DayOfWeek.Wednesday: return checkAllowed.Wednesday;
                case DayOfWeek.Thursday: return checkAllowed.Thursday;
                case DayOfWeek.Friday: return checkAllowed.Friday;
                case DayOfWeek.Saturday: return checkAllowed.Saturday;
                case DayOfWeek.Sunday: return checkAllowed.Sunday;
                default: return false;
            }
        }
    }
}

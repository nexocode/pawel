﻿namespace AvailabilityParsing.Models
{
    using System;

    public class AvailabilityParsingEnumConverterException : Exception
    {
        public AvailabilityParsingEnumConverterException(string message) : base(message)
        {
        }
    }
}

﻿using AvailabilityParsing.Extensions;
using AvailabilityParsing.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AvailabilityParsing.Services
{
    public static class TransformToAvailabilitiesService
    {
        private static readonly int MaxLengthOfStay = Common.Consts.MAX_LENGHT_OF_STAY;
        private static readonly int MinLengthOfStay  = Common.Consts.MIN_LENGHT_OF_STAY;

        /// <summary>
        /// Transforms given to booking periods and cicos availabilities of property
        /// <param name="availabilites">List of booked periods</param>
        /// <param name="cicos">Check in/check out rules</param>
        /// <param name="maxDateTime">max datetime to check</param>
        /// <returns> ILookup<DateTime, int> </returns>
        public static ILookup<DateTime, int> TransformToAvailabilities(List<Availability> availabilites, List<Cico> cicos, DateTime? maxDateTime = null)
        {
            var list = new List<KeyValuePair<DateTime, int>>();
                        
            var currentDay = DateTime.Today; // first day in period to check
            var lastDay = maxDateTime.HasValue // last day of period to check
                ? maxDateTime.Value 
                : currentDay.AddMonths(Common.Consts.MAX_AVAILABILITY_MONTHS);

            
            while(currentDay <= lastDay) 
            {
                ProcessAvailabilitiesForDay(currentDay, lastDay, availabilites, cicos, list);
                currentDay = currentDay.AddDays(1);
            }

            return list.ToLookup(x => x.Key, x => x.Value);
        }

        private static void ProcessAvailabilitiesForDay(DateTime currentDay, DateTime lastDay, List<Availability> bookedPeriods, List<Cico> cicos, List<KeyValuePair<DateTime, int>> availabilityList)
        {
            var bookedPeriod = IsInBookedPeriod(currentDay, bookedPeriods);
            if (bookedPeriod != null || CheckInIsAllowedByCico(currentDay, cicos) == false)
            {
                return;
            }

            CreateAvailabilities(currentDay, lastDay, bookedPeriods, cicos, availabilityList);
        }

        private static void CreateAvailabilities(DateTime currentDay, DateTime lastDay, List<Availability> bookedPeriods, List<Cico> cicos, List<KeyValuePair<DateTime, int>> availabilityList)
        {
            var firstBookingPeriod = bookedPeriods?.FirstOrDefault(x => x.Period?.PeriodStart >= currentDay);
          
            for(int i = MinLengthOfStay; i<= MaxLengthOfStay; i++)
            {
                var checkoutDay = currentDay.AddDays(i);

                if(firstBookingPeriod != null && checkoutDay > firstBookingPeriod.Period.PeriodStart) // there is booking - further iterations does not make sense 
                {
                    return;
                }
                
                if(CheckOutIsAllowedByCico(checkoutDay, cicos) == false) // check out not allowed by cico
                {
                    continue;
                }

                availabilityList.Add(new KeyValuePair<DateTime, int>(currentDay, i));
            }
        }
        
        private static bool CheckInIsAllowedByCico(DateTime currentDay, List<Cico> cicos)
        {
           var cico = cicos?.FirstOrDefault(x => x.StartDate <= currentDay && x.EndDate >= currentDay);
           return cico == null ? true : currentDay.IsDayAllowed(cico.CheckInAllowed);
        }

        private static bool CheckOutIsAllowedByCico(DateTime currentDay, List<Cico> cicos)
        {
            var cico = cicos?.FirstOrDefault(x => x.StartDate <= currentDay && x.EndDate >= currentDay);
            return cico == null ? true : currentDay.IsDayAllowed(cico.CheckOutAllowed);
        }

        private static Availability IsInBookedPeriod(DateTime currentDay, List<Availability> bookedPeriods)
        {
            if(bookedPeriods == null)
            {
                return null;
            }

            return bookedPeriods.FirstOrDefault(x => x.Period.PeriodStart <= currentDay && x.Period.PeriodEnd > currentDay);
        }
    }
}

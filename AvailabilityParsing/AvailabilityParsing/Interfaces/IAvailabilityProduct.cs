﻿using System;
using System.Linq;

namespace AvailabilityParsing.Interfaces
{
    public interface IAvailabilityProduct
    {
        /// <summary>
        /// Generates collection of availabilities
        /// </summary>
        /// <param name="maxDateTime">Max datetime scope</param>
        /// <returns>ILookup<DateTime, int></returns>
        ILookup<DateTime, int> TransformToAvailabilities(DateTime? maxDateTime = null);
    }
}

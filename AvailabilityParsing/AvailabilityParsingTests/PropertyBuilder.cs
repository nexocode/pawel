﻿using AvailabilityParsing.Models;
using System.Collections.Generic;
using System;

namespace AvailabilityParsingTests
{
    /// <summary>
    /// Separates creation of property 
    /// </summary>
    public class PropertyBuilder
    {
        private List<string> _amenities = new List<string>();
        List<Availability> _avialability;
        List<Cico> _cicos;

        public Property Build()
        {
            return new Property
            {
                Cico = _cicos,

                Availability = _avialability
            };
        }

        public PropertyBuilder WithCicos(List<Cico> cicos)
        {
            _cicos = cicos;
            return this;
        }

        public PropertyBuilder WithOneAvailability(DateTime startDate, DateTime endDate)
        {
            _avialability = new List<Availability>
            {
                new Availability
                {
                    Period = new Period
                    {
                        PeriodEnd = endDate,
                        PeriodStart = startDate
                    }
                }
            };
            return this;
        }

        public PropertyBuilder WithAvailabilities(List<Availability> avialability)
        {
            _avialability = avialability;
            return this;
        }
    }
}

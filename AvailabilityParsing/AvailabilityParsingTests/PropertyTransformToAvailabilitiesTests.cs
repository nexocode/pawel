﻿namespace AvailabilityParsingTests
{
    using NUnit.Framework;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using AvailabilityParsing.Models;

    /// <summary>
    /// Tests of Property.TransformToAvailabilities 
    /// </summary>
    [TestFixture]
    public class PropertyTransformToAvailabilitiesTests
    {
        [Test]
        public void When_AvailabilitiesAndCicosEmpty_And_MaxDayIsPlusThreeDays_Then_LookupContainsFourElements()
        {
            var builder = new PropertyBuilder();
            var property = builder.Build();

            //default lenght of stay is Consts.MIN_LENGHT_OF_STAY
            var availabilities = property.TransformToAvailabilities(DateTime.Now.AddDays(3));
                 
            Assert.AreEqual(4, availabilities.Count);
        }

        [Test]
        public void When_AvailabilitiesAndCicosEmpty_Then_LookupElementCount_IsMaxLenghtOfStay()
        {
            var builder = new PropertyBuilder();
            var property = builder.Build();

            //default lenght of stay is Consts.MIN_LENGHT_OF_STAY
            var availabilities = property.TransformToAvailabilities(DateTime.Now.AddDays(3));

            Assert.AreEqual(availabilities[DateTime.Now.Date].Count(), AvailabilityParsing.Common.Consts.MAX_LENGHT_OF_STAY);
        }
                     
        [Test, TestCaseSource(typeof(PropertyTransformTestDataClass), "OneDayBookedPeriod")]
        public void When_PropertyIsBookedForOneDay_And_Then_LookupContainsProperElements(DateTime maxDateTime, DateTime bookedFrom, DateTime bookedTo,int resultPeriodsCount)
        {
          
            var builder = new PropertyBuilder().WithOneAvailability(bookedFrom, bookedTo);
            var property = builder.Build();

            //default lenght of stay is Consts.MIN_LENGHT_OF_STAY
            var availabilities = property.TransformToAvailabilities(maxDateTime);

            Assert.AreEqual(resultPeriodsCount, availabilities.Count);
        }

        [Test]
        public void When_ThereAreTwoBookingPeriods_Then_LookupContainsOneElements()
        {
            var bookings = new List<Availability>
            {
                new Availability
                {
                    Period = new Period
                    {
                        PeriodStart = DateTime.Now.Date.AddDays(1),
                        PeriodEnd = DateTime.Now.Date.AddDays(2)
                    }
                },
                new Availability
                {
                     Period = new Period
                    {
                        PeriodStart = DateTime.Now.Date.AddDays(2),
                        PeriodEnd = DateTime.Now.Date.AddDays(4)
                    }
                }
            };

            var builder = new PropertyBuilder().WithAvailabilities(bookings);
            var property = builder.Build();

            //default lenght of stay is Consts.MIN_LENGHT_OF_STAY
            var availabilities = property.TransformToAvailabilities(DateTime.Now.AddDays(3));

            Assert.AreEqual(availabilities.Count, 1);
        }

        [Test]
        public void When_PropertyBookedFromNextDay_And_CicosIsTrueForCurrentDay_And_Then_LookupContainsOneElement()
        {
            var cicos = new List<Cico>
            {
                new Cico
                {
                    CheckInAllowed = new CheckAllowed
                    {
                        Monday = DateTime.Now.Date.DayOfWeek == DayOfWeek.Monday,
                        Tuesday = DateTime.Now.Date.DayOfWeek == DayOfWeek.Tuesday,
                        Wednesday = DateTime.Now.Date.DayOfWeek == DayOfWeek.Wednesday,
                        Thursday = DateTime.Now.Date.DayOfWeek == DayOfWeek.Thursday,
                        Friday = DateTime.Now.Date.DayOfWeek == DayOfWeek.Friday,
                        Saturday = DateTime.Now.Date.DayOfWeek == DayOfWeek.Saturday,
                        Sunday = DateTime.Now.Date.DayOfWeek == DayOfWeek.Sunday,
                    },
                    StartDate = DateTime.Now.Date.AddDays(-10),
                    EndDate = DateTime.Now.Date.AddDays(5)
                }
            };
            var builder = new PropertyBuilder()
                .WithOneAvailability(DateTime.Now.Date.AddDays(1), DateTime.Now.Date.AddDays(8))
                .WithCicos(cicos);
            var property = builder.Build();

            //default lenght of stay is Consts.MIN_LENGHT_OF_STAY
            var availabilities = property.TransformToAvailabilities(DateTime.Now.Date.AddDays(3));

            Assert.AreEqual(1, availabilities.Count);
        }

        [Test]
        public void When_PropertyBookedFromNextDay_And_CicosCheckOutIsFalseFortomorow_And_Then_LookupContainsZeroElement()
        {
            var cicos = new List<Cico>
            {
                new Cico
                {
                    CheckOutAllowed = new CheckAllowed
                    {
                        Monday = false,
                        Tuesday =false,
                        Wednesday = false,
                        Thursday = false,
                        Friday = false,
                        Saturday = false,
                        Sunday =false,
                    },
                    StartDate = DateTime.Now.Date.AddDays(-10),
                    EndDate = DateTime.Now.Date.AddDays(5)
                }
            };
            var builder = new PropertyBuilder()
                .WithOneAvailability(DateTime.Now.Date.AddDays(1), DateTime.Now.Date.AddDays(8))
                .WithCicos(cicos);
            var property = builder.Build();

            //default lenght of stay is Consts.MIN_LENGHT_OF_STAY
            var availabilities = property.TransformToAvailabilities(DateTime.Now.Date.AddDays(3));

            Assert.AreEqual(0, availabilities.Count);
        }
    }

    public class PropertyTransformTestDataClass
    {
        // Calendar periods e.g:  A B B A // where A -avialible B- Booked
        public static IEnumerable OneDayBookedPeriod
        {
            get
            {
                yield return new TestCaseData(DateTime.Now.Date.AddDays(3), DateTime.Now.Date.AddDays(-5), DateTime.Now.Date.AddDays(-3), 4);// A A A A
                yield return new TestCaseData(DateTime.Now.Date.AddDays(3), DateTime.Now.Date.AddDays(1), DateTime.Now.Date.AddDays(2), 3);// A B B A
                yield return new TestCaseData(DateTime.Now.Date.AddDays(3), DateTime.Now.Date, DateTime.Now.Date.AddDays(1), 3);// B B A A
                yield return new TestCaseData(DateTime.Now.Date.AddDays(3), DateTime.Now.Date.AddDays(2), DateTime.Now.Date.AddDays(3), 3);// A A B B
                yield return new TestCaseData(DateTime.Now.Date.AddDays(3), DateTime.Now.Date.AddDays(1), DateTime.Now.Date.AddDays(3), 2);// A B B B
                yield return new TestCaseData(DateTime.Now.Date.AddDays(3), DateTime.Now.Date, DateTime.Now.Date.AddDays(3), 1);// B B B B - 1 because from the last booked day new booking can start

            }
        }
    }
}

Availability parsing service
=========

This code presents simple solution for creating availability calendar for a given property.
Availabilities are build based on booking periods and check in/check out rules.
Example 'Property' model implements IAvailabilityProduct interface which provides Availabilities generation.

### Getting started ###

```csharp
var property = new Property();
var availabilities = property.TransformToAvailabilities(DateTime.Now.AddDays(1));
```